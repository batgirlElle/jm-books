
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<jsp:useBean id="book" type="model.books.BookItem" scope="request"></jsp:useBean>
<jsp:useBean id="movie" type="model.movies.MovieItem" scope="request"></jsp:useBean>
<jsp:useBean id="coffee" type="model.coffee.CoffeeItem" scope="request"></jsp:useBean>

<!DOCTYPE html>
<html>
<head>
<title>JM Books Inc.</title>
<meta charset="UTF-8">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

</head>

<body>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="page-header">
				<h3 class="w3-wide">
					<img src="images/logo.jpg" alt="JM Logo" height="250px">
				</h3>
				<h1>Order Details</h1>
			</div>

			<div class="row justify-content-md-center">
				<div class="jumbotron">
					<div class="container">
						<h1>Books</h1>


						<div class="col-xs-1 col-md-1&gt;">
							<button class="btn btn-default btn-lg dropdown-toggle"
								type="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false" onclick="window.history.back()">
								Back</button>
						</div>

					</div>


					<div class="row justify-content-md-center">
						<div class="col-xs-7 col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading"><%=book.name()%>
								</div>
								<div class="panel-body">
									<p>
										Author:
										<%=book.author()%>
									</p>
									<p>
										Genre:
										<%=book.genre()%>
									</p>
									<p>
										Price:
										P<%=book.price()%>
									</p>
									<p>
										Packing:
										<%=book.packing().pack()%>
									</p>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">Description</div>
								<div class="panel-body">
									<p>
										<%=book.description()%>
									</p>
								</div>
							</div>
						</div>

						<div class="col-xs-4 col-md-4">
							<a href="#" class="thumbnail"> <img
								src="./images/<%=book.picture()%>" alt="<%=book.name()%>">
								<span class="label label-default"><%=book.name()%></span>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="row justify-content-md-center">

				<div class="jumbotron">
					<div class="container">
						<h1>Movies</h1>

						<div class="col-xs-1 col-md-1&gt;">
							<button class="btn btn-default btn-lg dropdown-toggle"
								type="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false" onclick="window.history.back()">
								Back</button>
						</div>
					</div>

					<div class="row justify-content-md-center">
						<div class="col-xs-7 col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading"><%=movie.title()%></div>
								<div class="panel-body">
									<p>
										Director:
										<%=movie.director()%>
									</p>
									<p>
										Genre:
										<%=movie.genre()%>
									</p>
									<p>
										Price:
										P<%=movie.price()%>
									</p>
									<p>
										Packing:
										<%=movie.packing().pack()%>
									</p>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">Description</div>
								<div class="panel-body">
									<p>
										<%=movie.description()%>
									</p>
								</div>
							</div>
						</div>

						<div class="col-xs-4 col-md-4">
							<a href="#" class="thumbnail"> <img
								src="./images/<%=movie.picture()%>" alt="<%=movie.title()%>">
								<span class="label label-default"><%=movie.title()%></span>
							</a>
						</div>
					</div>

				</div>
			</div>

			<div class="row justify-content-md-center">

				<div class="jumbotron">
					<div class="container">
						<h1>Coffee</h1>

						<div class="col-xs-1 col-md-1&gt;">
							<button class="btn btn-default btn-lg dropdown-toggle"
								type="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false" onclick="window.history.back()">
								Back</button>
						</div>
					</div>

					<div class="row justify-content-md-center">
						<div class="col-xs-7 col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading"><%=coffee.type()%></div>
								<div class="panel-body">
									<p>
										Price:
										P<%=coffee.price()%>
									</p>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">Description</div>
								<div class="panel-body">
									<p>
										Size:
										<%=coffee.size()%>
									</p>
									<p>
										Type:
										<%=coffee.type()%>
									</p>
									<p>
										Sugar Level:
										<%=coffee.sugarLevel()%>%
									</p>
									<p>
										Packing:
										<%=coffee.packing().pack()%>
									</p>
								</div>
							</div>
						</div>

						<div class="col-xs-4 col-md-4">
							<a href="#" class="thumbnail"> <img
								src="./images/<%=coffee.picture()%>" alt="<%=coffee.type()%>">
								<span class="label label-default"><%=coffee.type()%></span>
							</a>
						</div>
					</div>

					<!-- OUTPUT OF TOTAL COST! -->

					<div class="row justify-content-md-center">
						<div class="col-xs-7 col-md-7">
							<div class="panel panel-default">
								<div class="panel-heading"><b>TOTAL COST</b></div>
								<div class="panel-body">
									<p>
										<% double cost = book.price() + movie.price() + coffee.price(); %>
										P<%= cost %>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>