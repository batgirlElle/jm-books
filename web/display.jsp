<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>JM Books Inc.</title>
<meta charset="UTF-8">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

</head>
<body>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="page-header">
				<img src="images/logo.jpg" alt="JM Logo" height="250px">

				<h1>Ordering Content</h1>
			</div>

		</div>

		<form id="buy" action="./purchase.action" method="post">
			<div class="row justify-content-md-center">
				<div class="jumbotron">
					<div class="container">
						<h1>Books</h1>

						<div class="row">
							<div class="col-xs-6 col-md-3">
								<a href="#" onclick="selectbook(0)" class="thumbnail"> <img
									src="./images/origin.jpg" alt="Origin">
								</a>
							</div>
							<div class="col-xs-6 col-md-3">
								<a href="#" onclick="selectbook(1)" class="thumbnail"> <img
									src="./images/artemis.jpg" alt="Artemis">
								</a>
							</div>
							<div class="col-xs-6 col-md-3">
								<a href="#" onclick="selectbook(2)" class="thumbnail"> <img
									src="./images/blame.jpg" alt="Blame">
								</a>
							</div>
						</div>

						<div class="form-group">
							<label for="sel1"><font size="20px">Select a book:</font></label>
							<select class="form-control" id="bookName" name="bookName">
								<option>Origin</option>
								<option>Artemis</option>
								<option>Blame</option>
							</select>
						</div>
					</div>
				</div>
				<p></p>
			</div>

			<div class="row justify-content-md-center">
				<div class="jumbotron">
					<div class="container">
						<h1>Movies</h1>

						<div class="row">
							<div class="col-xs-6 col-md-3">
								<a href="#" onclick="selectmovie(0)" class="thumbnail"> <img
									src="./images/neversaynever.jpg" alt="NeverSayNever">
								</a>
							</div>
							<div class="col-xs-6 col-md-3">
								<a href="#" onclick="selectmovie(1)" class="thumbnail"> <img
									src="./images/tangled.jpg" alt="Tangled">
								</a>
							</div>
							<div class="col-xs-6 col-md-3">
								<a href="#" onclick="selectmovie(2)" class="thumbnail"> <img
									src="./images/theavengers.jpg" alt="The Avengers">
								</a>
							</div>
						</div>

						<div class="form-group">
							<label for="sel1"><font size="20px">Select movie:</font></label>
							<select class="form-control" id="movieTitle" name="movieTitle">
								<option>Never Say Never</option>
								<option>Tangled</option>
								<option>The Avengers</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row justify-content-md-center">
					<div class="jumbotron">
						<div class="container">
							<h1>Coffee</h1>

							<div class="row">
								<div class="col-xs-6 col-md-3">
									<a href="#" onclick="selectcoffee(0)" class="thumbnail"> <img
										src="./images/americano.jpg" alt="Americano">
									</a>
								</div>
								<div class="col-xs-6 col-md-3">
									<a href="#" onclick="selectcoffee(1)" class="thumbnail"> <img
										src="./images/black.jpg" alt="Black">
									</a>
								</div>
								<div class="col-xs-6 col-md-3">
									<a href="#" onclick="selectcoffee(2)" class="thumbnail"> <img
										src="./images/espresso.jpg" alt="Expresso">
									</a>
								</div>
							</div>

							<div class="form-group">
								<label for="sel1"><font size="20px">Select type
										of coffee:</font></label> <select class="form-control" id="coffeeType"
									name="coffeeType">
									<option>Americano</option>
									<option>Black</option>
									<option>Espresso</option>
								</select> <br/>
								<button type="submit" class="btn btn-primary btn-lg">BUY
									ALL!</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>