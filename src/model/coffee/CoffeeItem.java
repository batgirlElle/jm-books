package model.coffee;

import utility.packing.Packing;

public interface CoffeeItem {

	public String type();
	public Packing packing();
	public String size();
	public String isHot();
	public int sugarLevel();
	public double price();
	public String picture();
}
