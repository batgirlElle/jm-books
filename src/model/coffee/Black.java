package model.coffee;

public class Black extends Coffee {

	@Override
	public String type() {
		return "Black Coffee";
	}

	@Override
	public String size() {
		return "Tall";
	}

	@Override
	public String isHot() {
		return "Hot";
	}

	@Override
	public int sugarLevel() {
		return 10;
	}

	@Override
	public double price() {
		return 130.00;
	}

	@Override
	public String picture() {
		return "black.jpg";
	}
}