package model.coffee;

public class Americano extends Coffee {

	@Override
	public String type() {
		return "Americano";
	}

	@Override
	public String size() {
		return "Tall";
	}

	@Override
	public String isHot() {
		return "Hot";
	}

	@Override
	public int sugarLevel() {
		return 5;
	}

	@Override
	public double price() {
		return 150.00;
	}

	@Override
	public String picture() {
		return "americano.jpg";
	}
}