package model.coffee;

import utility.packing.*;

public abstract class Coffee implements CoffeeItem {

	public abstract String type();
	
	@Override
	public Packing packing() {
		return new Styrofoam();
	}
	
	public abstract String size();
	public abstract String isHot();
	public abstract int sugarLevel();
	public abstract double price();
	public abstract String picture();
}