package model.coffee;

public class Espresso extends Coffee {

	@Override
	public String type() {
		return "Espresso";
	}

	@Override
	public String size() {
		return "Grande";
	}

	@Override
	public String isHot() {
		return "Cold";
	}

	@Override
	public int sugarLevel() {
		return 3;
	}

	@Override
	public double price() {
		return 200.00;
	}

	@Override
	public String picture() {
		return "espresso.jpg";
	}
}