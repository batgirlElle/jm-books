package model.movies;

public class TheAvengers extends Movie {

	@Override
	public String title() {
		return "The Avengers";
	}

	@Override
	public String genre() {
		return "Fantasy/Science fiction";
	}
	
	@Override
	public String director() {
		return "Joss Whedon";
	}

	@Override
	public String description() {
		return "Nick Fury is the director of S.H.I.E.L.D., an "
			+ "international peace-keeping agency. The agency is a who's "
			+ "who of Marvel Super Heroes, with Iron Man, The Incredible Hulk, Thor, "
			+ "Captain America, Hawkeye and Black Widow. When global security is threatened"
			+ " by Loki and his cohorts, Nick Fury and his team will need all their powers "
			+ "to save the world from disaster which is formed by Loki and his team";
	}
	
	@Override
	public double price() {
		return 350.00;
	}

	@Override
	public String picture() {
		return "theavengers.jpg";
	}
}