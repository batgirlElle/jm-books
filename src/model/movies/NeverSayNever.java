package model.movies;

public class NeverSayNever extends Movie {

	@Override
	public String title() {
		return "Never Say Never";
	}

	@Override
	public String genre() {
		return "Rockumentary/Music";
	}
	
	@Override
	public String director() {
		return "Jon M. Chu";
	}

	@Override
	public String description() {
		return "It is a documentary-type of movie about Justin Bieber's road to fame.";
	}
	
	@Override
	public double price() {
		return 250.00;
	}

	@Override
	public String picture() {
		return "neversaynever.jpg";
	}
}