package model.movies;

import utility.packing.*;

public abstract class Movie implements MovieItem {

	public abstract String title();
	
	@Override
	public Packing packing() {
		return new Plastic();
	}

	public abstract String genre();
	public abstract String director();
	public abstract String description();
	public abstract double price();
	public abstract String picture();
}