package model.movies;

import utility.packing.Packing;

public interface MovieItem {
	
	public String title();
	public Packing packing();
	public String genre();
	public String director();
	public String description();
	public double price();
	public String picture();
}