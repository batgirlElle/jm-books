package model.movies;

public class Tangled extends Movie {

	@Override
	public String title() {
		return "Tangled";
	}

	@Override
	public String genre() {
		return "Fantasy/Comedy music";
	}
	
	@Override
	public String director() {
		return "Nathan Greno and Byron Howard";
	}

	@Override
	public String description() {
		return "The only access to the world Rapunzel has is a wide "
			+ "window from which Mother Gothel comes and goes, "
			+ "using Rapunzel's 70 foot long hair as a rope ladder.";
	}
	
	@Override
	public double price() {
		return 200.00;
	}

	@Override
	public String picture() {
		return "tangled.jpg";
	}
}