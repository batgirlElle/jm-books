package model.books;

public class Origin extends Book {

	@Override
	public String name() {
		return "Origin";
	}

	@Override
	public String author() {
		return "Dan Brown";
	}

	@Override
	public String genre() {
		return "Mystery";
	}

	@Override
	public double price() {
		return 876.00;
	}

	@Override
	public String description() {
		return "A personified supercomputer with a British accent manages the plot of Origin by issuing instructions from an iPhone";
	}

	@Override
	public String picture() {
		return "origin.jpg";
	}

}
