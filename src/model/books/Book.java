package model.books;

import utility.packing.*;

public abstract class Book implements BookItem {

	@Override
	public abstract String name();

	@Override
	public Packing packing() {
		return new Paper();
	}

	@Override
	public abstract String author();

	@Override
	public abstract String genre();

	@Override
	public abstract double price();

	@Override
	public abstract String description();

	@Override
	public abstract String picture();
}