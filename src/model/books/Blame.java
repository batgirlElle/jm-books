package model.books;

public class Blame extends Book {

	@Override
	public String name() {
		return "Blame";
	}

	@Override
	public String author() {
		return "Jeff Abbott";
	}

	@Override
	public String genre() {
		return "Action and Adventure";
	}

	@Override
	public double price() {
		return 777.50;
	}

	@Override
	public String description() {
		return "Sometimes the person you thought you knew best...Turns out to be someone you never really knew at all.";
	}

	@Override
	public String picture() {
		return "blame.jpg";
	}
}