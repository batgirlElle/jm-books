package model.books;

public class RedSister extends Book {

	@Override
	public String name() {
		return "Red Sister";
	}

	@Override
	public String author() {
		return "Mark Lawrence";
	}

	@Override
	public String genre() {
		return "Fantasy";
	}

	@Override
	public double price() {
		return 350.00;
	}

	@Override
	public String description() {
		return "I was born for killing � the gods made me to ruin.\r\n At the Convent of Sweet Mercy young girls are raised to be killers. In a few the old bloods show, gifting talents rarely seen since the tribes beached their ships on Abeth. Sweet Mercy hones its novices� skills to deadly effect: it takes ten years to educate a Red Sister in the ways of blade and fist. ";
	}

	@Override
	public String picture() {
		return "redsister.jpg";
	}

}
