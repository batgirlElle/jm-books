package model.books;

public class Artemis extends Book {

	@Override
	public String name() {
		return "Artemis";
	}

	@Override
	public String author() {
		return "Andy Weir";
	}

	@Override
	public String genre() {
		return "Action and Adventure";
	}

	@Override
	public double price() {
		return 806.00;
	}

	@Override
	public String description() {
		return "A heist story set on the moon.";
	}

	@Override
	public String picture() {
		return "artemis.jpg";
	}
}