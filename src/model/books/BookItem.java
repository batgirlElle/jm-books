package model.books;

import utility.packing.Packing;

public interface BookItem {

	public String name();
	public Packing packing();
	public String author();
	public String genre();
	public double price();
	public String description();
	public String picture();
}