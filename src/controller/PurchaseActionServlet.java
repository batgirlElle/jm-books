package controller;

import java.io.IOException;
import java.sql.Connection;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.db.*;
import model.books.*;
import model.movies.*;
import model.coffee.*;
import utility.facade.*;

@WebServlet("/purchase.action")
public class PurchaseActionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	Connection connection = null;

	public void init() throws ServletException {
		//connection = DBSingletonConnection.getConnection();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String bookName = request.getParameter("bookName");
		String movieTitle = request.getParameter("movieTitle");	
		String coffeeType = request.getParameter("coffeeType");	
		
//		ProductBuilder productBuilder = new ProductBuilder();
//		
//		BookProduct book = productBuilder.prepareBook(bookProduct);
//		MovieProduct movie = productBuilder.prepareMovie(movieProduct);
//		CoffeeProduct coffee = productBuilder.prepareCoffee(coffeeProduct);
//				
//			BookItem book = ProductBuilder.prepareBook(bookName);
//			MovieItem movie = ProductBuilder.prepareMovie(movieTitle);
//			CoffeeItem coffee = ProductBuilder.prepareCoffee(coffeeType);
		
		//facade
			ShopKeeper shop = new ShopKeeper();
			BookItem book = shop.bookSale(bookName);
			MovieItem movie = shop.movieSale(movieTitle);
			CoffeeItem coffee = shop.coffeeSale(coffeeType);
			
			//now insert into DB
			if (DBSQLOperation.insertBook(book, connection)) {
				System.out.println("book inserted to db successfully");
			} else {
				System.err.println("book did not insert");
			}
					
			//now insert into DB
			if (DBSQLOperation.insertMovie(movie, connection)) {
				System.out.println("movie inserted to db successfully");
			} else {
				System.err.println("movie did not insert");
			}
			
			//now insert into DB
			if (DBSQLOperation.insertCoffee(coffee, connection)) {
				System.out.println("coffee inserted to db successfully");
			} else {
				System.err.println("coffee did not insert");
			}
			
			//bind on request
			request.setAttribute("book", book);
			request.setAttribute("movie", movie);
			request.setAttribute("coffee", coffee);
			
			request.getRequestDispatcher("purchased.jsp").forward(request, response);
	}
}