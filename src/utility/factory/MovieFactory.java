package utility.factory;

import model.books.*;
import model.coffee.*;
import model.movies.*;

public class MovieFactory implements AbstractFactory {

	public MovieItem createMovie(String movieTitle) {
		MovieItem movie = null;
		
		switch(movieTitle.toUpperCase()){
		case "NEVER SAY NEVER":
			movie = new NeverSayNever();
			break;
		case "THE AVENGERS":
			movie = new TheAvengers();
			break;
		case "TANGLED":
			movie = new Tangled();
			break;
		default:
			System.err.println("MovieFactory received an incorrect order");
		}
		return movie;
	}
	
	@Override
	public BookItem createBook(String bookName) {
		return null;
	}
	
	@Override
	public CoffeeItem createCoffee(String coffeeType) {
		return null;
	}
}
