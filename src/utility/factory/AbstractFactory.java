package utility.factory;

import model.movies.*;
import model.books.*;
import model.coffee.*;

public interface AbstractFactory {

	public MovieItem createMovie (String movieTitle);
	public BookItem createBook (String bookName);
	public CoffeeItem createCoffee (String coffeeType);
}