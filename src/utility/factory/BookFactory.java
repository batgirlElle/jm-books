package utility.factory;

import model.books.*;
import model.coffee.*;
import model.movies.*;

public class BookFactory implements AbstractFactory {
	public BookItem createBook(String bookName) {
		
		if(bookName == null) {
			
			return null;
		}else if(bookName.equalsIgnoreCase("Origin")){
			return new Origin();
		}else if(bookName.equalsIgnoreCase("Blame")) {
			return new Blame();
		}else if(bookName.equalsIgnoreCase("RedSister")) {
			return new RedSister();
		}else if(bookName.equalsIgnoreCase("Artemis")) {
			return new Artemis();
		}
		System.err.println("BookFactory received an incorrect order");
		return null;
	}

	@Override
	public MovieItem createMovie(String movieTitle) {
		return null;
	}

	@Override
	public CoffeeItem createCoffee(String coffeeType) {
		return null;
	}
}