package utility.factory;

import model.books.*;
import model.coffee.*;
import model.movies.*;

public class CoffeeFactory implements AbstractFactory {

	public CoffeeItem createCoffee (String coffeeType) {
		CoffeeItem coffee = null;
		
		switch(coffeeType.toUpperCase()) {
			case "ESPRESSO":
				coffee = new Espresso();
				break;
			case "AMERICANO":
				coffee = new Americano();
				break;
			case "BLACK":
				coffee = new Black();
				break;
			default:
				System.err.println("CoffeeFactory received an incorrect order");
			}
			return coffee;
	}
	
	@Override
	public BookItem createBook(String bookName) {
		return null;
	}
	
	@Override
	public MovieItem createMovie(String movieTitle) {
		return null;
	}
}
