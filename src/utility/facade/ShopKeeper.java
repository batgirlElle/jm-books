package utility.facade;

import model.books.BookItem;
import model.coffee.CoffeeItem;
import model.movies.MovieItem;
import utility.builder.ProductBuilder;

public class ShopKeeper {
	private ProductBuilder prodBook = new ProductBuilder();
	private ProductBuilder prodMovie = new ProductBuilder();
	private ProductBuilder prodCoffee = new ProductBuilder();
	
	public ShopKeeper() {
		
	}
	
	public BookItem bookSale(String bookName) {
		return (BookItem) prodBook.prepareBook(bookName);
	}
	
	public MovieItem movieSale(String movieTitle) {
		return (MovieItem) prodMovie.prepareMovie(movieTitle);
	}
	
	public CoffeeItem coffeeSale(String coffeeType) {
		return (CoffeeItem) prodCoffee.prepareCoffee(coffeeType);
	}
}