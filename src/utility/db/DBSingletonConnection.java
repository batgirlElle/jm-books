package utility.db;

import java.sql.*;

public class DBSingletonConnection {

	private static Connection connection;
	
	private DBSingletonConnection() {
	
	}
	
	private static Connection getDBConnection() {
	    String url= "jdbc:mysql://localhost:3306/";
	        String dbName = "despatt-malixi-se31-db-hw";
	        String driver = "com.mysql.jdbc.Driver";
	        String userName = "root";
	        String password = "";
	        try {
	            Class.forName(driver).newInstance();
	            connection = (Connection)DriverManager.getConnection(url+dbName,userName,password);
	        } catch (ClassNotFoundException nfe) {
	        	System.err.println(nfe.getMessage());	        
	        }catch (Exception sqle) {
	            sqle.printStackTrace();
	        }
	   
	   return connection;
	}
	
	public static Connection getConnection() {
		if(connection!=null) {
			getDBConnection();
			System.out.println(connection.toString());
		}else {
			System.out.println("Connection is NULL");
		}
		return ((connection==null)?getDBConnection():connection);
	}
}