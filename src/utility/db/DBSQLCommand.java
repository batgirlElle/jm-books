package utility.db;

public interface DBSQLCommand {

	String INSERT_BOOK = "insert into "
		+ "books(author,description,genre,"
		+ "price,name,picture,packing)"
		+ "values (?,?,?,?,?,?,?)";
	
	String INSERT_MOVIE = "insert into "
			+ "movies(title,genre,director,description,price,"
			+ "picture,packing)"
			+ "values (?,?,?,?,?,?,?)";
	
	String INSERT_COFFEE = "insert into "
			+ "coffee(type,size,isHot,"
			+ "price,sugarLevel,picture,packing)"
			+ "values (?,?,?,?,?,?,?)";
}
