package utility.db;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class Database implements Cloneable {

   private String id; //will serve as your key to a map
   protected Connection dbConnection;
   
   public Connection getDBConnection(){
	   String url= "jdbc:mysql://localhost:3306/";
       String dbName = "despatt-malixi-se31-db-hw";
       String driver = "com.mysql.jdbc.Driver";
       String userName = "root";
       String password = "";
       
       try {
           Class.forName(driver).newInstance();
           dbConnection = (Connection)DriverManager.getConnection(url+dbName,userName,password);
       } catch (ClassNotFoundException nfe) {
    	   System.err.println(nfe.getMessage());	        
       } catch (Exception sqle) {
           sqle.printStackTrace();
       }
       return dbConnection;
   }
   
   public String getId() {
      return id;
   }
   
   public void setId(String id) {
      this.id = id;
   }
   
   public Object clone() {
      Object clone = null;
      
      try {
         clone = super.clone();      
      } catch (CloneNotSupportedException e) {
         e.printStackTrace();
      }
      return clone;
   }
}