package utility.db;

import java.sql.*;

import model.books.*;
import model.movies.*;
import model.coffee.*;

public class DBSQLOperation implements DBSQLCommand {

	public static boolean insertBook(BookItem 
		book, Connection connection) {
		boolean isSuccessful = false;
		
		if (connection != null) {
			try {
				PreparedStatement pstmnt = 
					connection.prepareStatement(INSERT_BOOK);
				
				pstmnt.setString(1, book.author());
				pstmnt.setString(2, book.description());
				pstmnt.setString(3, book.genre());
				pstmnt.setDouble(4, book.price());
				pstmnt.setString(5, book.name());
				pstmnt.setString(6, book.picture());
				pstmnt.setString(7, book.packing().pack());
				
				pstmnt.executeUpdate();
				isSuccessful = true;
			} catch (SQLException sqle) {
				System.err.println(sqle.getMessage());
				sqle.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			System.err.println("Connection is null");
		}
		return isSuccessful;
	}
	
	public static boolean insertMovie(MovieItem 
		movie, Connection connection) {
		boolean isSuccessful = false;
		
		if (connection != null) {
			try {
				PreparedStatement pstmnt = 
					connection.prepareStatement(INSERT_MOVIE);
				
				pstmnt.setString(1, movie.title());
				pstmnt.setString(2, movie.genre());
				pstmnt.setString(3, movie.director());
				pstmnt.setString(4, movie.description());
				pstmnt.setDouble(5, movie.price());
				pstmnt.setString(6, movie.picture());
				pstmnt.setString(7, movie.packing().pack());
				
				pstmnt.executeUpdate();
				
				isSuccessful = true;
			} catch (SQLException sqle) {
				System.err.println(sqle.getMessage());
				sqle.printStackTrace();
			}
		}else {
			System.err.println("Connection is null");
		}
		return isSuccessful;
	}
	
	public static boolean insertCoffee(CoffeeItem 
			coffee, Connection connection) {
			boolean isSuccessful = false;
			
			if (connection != null) {
				try {
					PreparedStatement pstmnt = 
						connection.prepareStatement(INSERT_COFFEE);
					
					pstmnt.setString(1, coffee.type());
					pstmnt.setString(2, coffee.size());
					pstmnt.setString(3, coffee.isHot());
					pstmnt.setDouble(4, coffee.price());
					pstmnt.setInt(5, coffee.sugarLevel());
					pstmnt.setString(6, coffee.picture());
					pstmnt.setString(7, coffee.packing().pack());
					
					pstmnt.executeUpdate();
					
					isSuccessful = true;
				} catch (SQLException sqle) {
					System.err.println(sqle.getMessage());
					sqle.printStackTrace();
				}
			}else {
				System.err.println("Connection is null");
			}
			return isSuccessful;
		}
}