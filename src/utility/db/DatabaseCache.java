package utility.db;

import java.util.*;
public class DatabaseCache {

   //acts as a container	
   private static Hashtable<String, Database> 
   	databaseMap  = new Hashtable<String, Database>(); //acts as storage

   public static Database getDBConnection(String shapeId) {
      Database cachedDatabase = databaseMap.get(shapeId);
      return (Database) cachedDatabase.clone();
   }

   public static void loadCache() {
	   
	   
//      Circle circle = new Circle();
//      circle.setId("1"); //key of circle
//      shapeMap.put(circle.getId(),circle);
//
//      Square square = new Square();
//      square.setId("2"); //key of square
//      shapeMap.put(square.getId(),square);
//
//      Rectangle rectangle = new Rectangle();
//      rectangle.setId("3"); //key of rectangle
//      shapeMap.put(rectangle.getId(), rectangle);
   }
}
