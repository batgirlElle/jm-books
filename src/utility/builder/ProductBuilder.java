package utility.builder;

import model.books.BookItem;
import model.coffee.CoffeeItem;
import model.movies.MovieItem;
import utility.factory.*;

public class ProductBuilder {

	public BookItem prepareBook(String bookName) {
		BookItem book = new BookFactory().createBook(bookName);
		
		BookFactory bf = new BookFactory();
		
		book = (BookItem) bf.createBook(bookName);
		
		return book;
	}
	
	public MovieItem prepareMovie(String movieTitle) {	
		MovieItem movie = new MovieFactory().createMovie(movieTitle);
		
		MovieFactory mf = new MovieFactory();
		
		movie = (MovieItem) mf.createMovie(movieTitle);
			
		return movie;
	}
	
	public CoffeeItem prepareCoffee(String coffeeType) {	
		CoffeeItem coffee = new CoffeeFactory().createCoffee(coffeeType);
		
		CoffeeFactory cf = new CoffeeFactory();
		
		coffee = (CoffeeItem) cf.createCoffee(coffeeType);
			
		return coffee;
	}
}